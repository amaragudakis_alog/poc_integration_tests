package als.backend.ittests;

import als.backend.ittests.client.FeedReplayClient;
import als.backend.ittests.client.dto.UploadReplayRequest;
import als.backend.ittests.mongo.entity.FeedMessage;
import als.backend.ittests.mongo.repository.FeedMessageRepository;
import als.backend.ittests.repository.EventsRepository;
import als.backend.ittests.utils.PageIterator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.StreamSupport;

@SpringBootTest
public class SampleTest extends Assertions {

	@Autowired
	private EventsRepository eventsRepository;

	@Autowired
	private FeedMessageRepository feedMessageRepository;

	@Autowired
	private FeedReplayClient feedReplayClient;

	@Test
	public void getEventByFeedCodeFound() {
		Long eventId = eventsRepository.getEventIdByFeedCodeSuffix("162368455299418");
		assertNotNull(eventId);
		assertEquals(16242985L, eventId);
	}

	@Test
	public void getEventByFeedCodeNotFound() {
		Long eventId = eventsRepository.getEventIdByFeedCodeSuffix("189162189712498");
		assertNull(eventId);
	}

	@Test
	public void getMongodbFeedMessages() throws Exception {
		Page<FeedMessage> feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(31887763L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(0, 200, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(11, feedMessages.getContent().size());

//		String response = feedReplayClient.uploadReplay(Instant.now().toEpochMilli(), feedMessages.getContent());
//		assertNotNull(response);
	}

	@Test
	public void getMongodbFeedMessagesPageable() {
		Page<FeedMessage> feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(31887763L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(0, 5, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(5, feedMessages.getContent().size());

		feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(31887763L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(1, 5, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(5, feedMessages.getContent().size());

		feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(31887763L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(2, 5, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(1, feedMessages.getContent().size());

		feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(31887763L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(3, 5, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(0, feedMessages.getContent().size());
	}

	@Test
	public void getMongodbUnknownEventId() {
		Page<FeedMessage> feedMessages = feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(489123489L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(0, 5, Sort.by("timestamp")));

		assertNotNull(feedMessages);
		assertNotNull(feedMessages.getContent());
		assertEquals(0, feedMessages.getContent().size());
	}

	@Test
	public void getReplayCountEvents() {
		Long eventCount = feedReplayClient.countEvents(FeedMessage.Type.BR_U.name());

		assertNotNull(eventCount);
		assertTrue(eventCount > 10);
	}

	@Test
	public void getMongodbFeedMessagesPageable_2() {
		long matchTime = Instant.now().toEpochMilli();
		Iterator<Page<FeedMessage>> pageIterator = new PageIterator<>(pageNumber -> feedMessageRepository.findByEventIdInAndMessageTypeIn(
				Collections.singletonList(32812927L),
				Collections.singletonList(FeedMessage.Type.BR_U),
				PageRequest.of(pageNumber, 5, Sort.by("timestamp")))
		);

		AtomicInteger pageInvocations = new AtomicInteger();
		AtomicLong replaySequence = new AtomicLong();
		Iterator<UploadReplayRequest> feedMessageIterator =
				StreamSupport.stream(Spliterators.spliteratorUnknownSize(pageIterator, 0), false)
				.peek(page -> {
					assertNotNull(page);
					assertNotNull(page.getContent());
					assertTrue(page.getNumber() < page.getTotalPages() - 1 ? page.getContent().size() == 5 : page.getContent().size() <= 5);
					System.out.println("Page: " + page);
					pageInvocations.incrementAndGet();
				})
				.map(page -> feedReplayClient.uploadReplay(matchTime, page.getContent()))
				.filter(Objects::nonNull)
				.flatMap(Collection::stream)
				.peek(uploadReplayRequest -> uploadReplayRequest.setSequenceNumber(replaySequence.incrementAndGet()))
				.iterator();

		assertEquals(0, pageInvocations.get());
//		System.out.println("Starting iterating");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(1, pageInvocations.get());
//		System.out.println("Iterating second element");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(1, pageInvocations.get());
//		System.out.println("Iterating third element");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(1, pageInvocations.get());
//		System.out.println("Iterating fourth element");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(1, pageInvocations.get());
//		System.out.println("Iterating fifth element");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(1, pageInvocations.get());
//		System.out.println("Iterating sixth element");
//		System.out.println("Fetched items: " + feedMessageIterator.next());
		sendNextMessage(feedMessageIterator);
		assertEquals(2, pageInvocations.get());
	}

	private void sendNextMessage(Iterator<UploadReplayRequest> replayRequestIterator) {
		UploadReplayRequest request = replayRequestIterator.next();
		System.out.println("Iterating element: " + request.getSequenceNumber());
		System.out.println("Fetched items: " + request);
		feedReplayClient.sendMessage(request, request.getSequenceNumber());
	}
}
