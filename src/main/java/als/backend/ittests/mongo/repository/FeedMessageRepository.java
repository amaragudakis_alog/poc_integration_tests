package als.backend.ittests.mongo.repository;

import als.backend.ittests.mongo.entity.FeedMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FeedMessageRepository extends MongoRepository<FeedMessage, String> {
	Page<FeedMessage> findByEventIdInAndMessageTypeIn(List<Long> eventIds, List<FeedMessage.Type> messageTypes, Pageable pageable);
}
