package als.backend.ittests.mongo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "feedMessage")
@CompoundIndexes({
		@CompoundIndex(name = "messageType_eventId_timestamp_idx", def = "{'messageType': 1, 'eventId': 1, 'timestamp': 1}")
})
@Data
public class FeedMessage {
	@Id
	private String id;
	@Indexed(expireAfterSeconds = 1209600)
	private LocalDateTime timestamp;
	private String body;
	@Indexed
	private Long eventId;
	private Type messageType;

	public enum Type {
		BR_U,
		BR_L,
		BR_F,
		OW_P,
		OW_L,
		BG_L,
		MG_L,
		SA_P,
		SA_L,
		RQ,
		IG,
		ST_L,
		ST_P
	}
}
