package als.backend.ittests.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "EVENTS")
public class Events {
	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "SPORTCODE")
	private String sportCode;

	@Column(name = "FEEDCODE")
	private String feedCode;
}
