package als.backend.ittests.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UploadReplayRequest {

	private String id;
	private LocalDateTime timestamp;
	private Object body;
	private Long eventId;
	private String messageType;

	@JsonIgnore
	private Long sequenceNumber;
}
