package als.backend.ittests.client;

import als.backend.ittests.client.dto.UploadReplayRequest;
import als.backend.ittests.mongo.entity.FeedMessage;
import als.backend.ittests.utils.ExceptionUtils;
import als.backend.ittests.utils.FileUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FeedReplayClient {

	private static final String REPLAY_COUNT_EVENTS_URI = "/replay/count-events?messageTypes={messageType}";
	private static final String REPLAY_UPLOAD_REPLAY_URI = "/replay/upload-replay?matchTime={matchTime}";
	private static final String REPLAY_SEND_MESSAGE_URI = "/replay/send-message";

	private final RestTemplate restTemplate;
	private final ObjectMapper objectMapper;

	public FeedReplayClient(RestTemplate feedReplayTemplate, ObjectMapper objectMapper) {
		this.restTemplate = feedReplayTemplate;
		this.objectMapper = objectMapper;
	}

	public long countEvents(String messageType) {
		Map<String, String> uriVariables = Map.of("messageType", messageType);
		List<?> response = restTemplate.getForObject(REPLAY_COUNT_EVENTS_URI, List.class, uriVariables);
		return response.size();
	}

	public List<UploadReplayRequest> uploadReplay(Long matchTime, List<FeedMessage> feedMessages) {
		Map<String, Object> uriVariables = Map.of("matchTime", matchTime);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		List<UploadReplayRequest> requestDto = feedMessages.stream()
				.map(feedMessage -> UploadReplayRequest.builder()
						.id(feedMessage.getId())
						.timestamp(feedMessage.getTimestamp())
						.body(ExceptionUtils.checkedIOException(() -> objectMapper.readValue(feedMessage.getBody(), Object.class)))
						.eventId(feedMessage.getEventId())
						.messageType(feedMessage.getMessageType().name())
						.build())
				.collect(Collectors.toList());

		String requestStr = ExceptionUtils.checkedIOException(() -> objectMapper.writeValueAsString(requestDto));

		Resource requestResource = FileUtils.getTestFile("feedMessages", requestStr);
		body.add("file-data", requestResource);
		try {
			ResponseEntity<String> response = restTemplate.postForEntity(REPLAY_UPLOAD_REPLAY_URI, body, String.class, uriVariables);
			if (response.getStatusCode().is2xxSuccessful()) {
				return ExceptionUtils.checkedIOException(() -> objectMapper.readValue(response.getBody(), new TypeReference<>() {}));
			}

			return null;
		} finally {
			ExceptionUtils.checkedIOException(() -> requestResource.getFile().delete());
		}
	}

	public void sendMessage(UploadReplayRequest message, Long sequence) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Sequence", sequence.toString());
		HttpEntity<UploadReplayRequest> entity = new HttpEntity<>(message, headers);
		restTemplate.postForEntity(REPLAY_SEND_MESSAGE_URI, entity, Void.class);
	}
}
