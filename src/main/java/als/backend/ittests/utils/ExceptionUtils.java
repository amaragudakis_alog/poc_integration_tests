package als.backend.ittests.utils;

import java.io.IOException;
import java.io.UncheckedIOException;

public class ExceptionUtils {

	@FunctionalInterface
	public interface CheckedIOExceptionRunnable {
		void execute() throws IOException;
	}

	@FunctionalInterface
	public interface CheckedIOExceptionProducer<T> {
		T execute() throws IOException;
	}

	public static void checkedIOException(CheckedIOExceptionRunnable r) {
		try {
			r.execute();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public static <T> T checkedIOException(CheckedIOExceptionProducer<T> r) {
		try {
			return r.execute();
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
}
