package als.backend.ittests.utils;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtils {

	public static Resource getTestFile(String filenamePrefix, String text) {
		return ExceptionUtils.checkedIOException(() -> {
			File file = File.createTempFile(filenamePrefix, null);
			Path path = file.toPath();
			Files.write(path, text.getBytes(StandardCharsets.UTF_8));
			return new FileSystemResource(path.toFile());
		});
	}
}
