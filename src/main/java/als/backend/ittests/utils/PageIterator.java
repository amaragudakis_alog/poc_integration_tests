package als.backend.ittests.utils;

import org.springframework.data.domain.Page;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class PageIterator<T> implements Iterator<Page<T>> {

	private Page<T> next;
	private int i;
	private Function<Integer, Page<T>> searchFunction;

	public PageIterator(Function<Integer, Page<T>> searchFunction) {
		this.next = null;
		this.i = 0;
		this.searchFunction = searchFunction;
	}

	@Override
	public boolean hasNext() {
		if (next == null) {
			next = searchFunction.apply(i++);
		}

		return !next.getContent().isEmpty();
	}

	@Override
	public Page<T> next() {
		if (next == null && !hasNext()) {
			throw new NoSuchElementException();
		} else {
			Page<T> c = next;
			next = null;
			return c;
		}
	}
}
