package als.backend.ittests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItTestsApplication.class, args);
	}
}
