package als.backend.ittests.repository;

import als.backend.ittests.entity.Events;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface EventsRepository extends CrudRepository<Events, Long> {

	@Query("SELECT e.id FROM Events e WHERE e.feedCode LIKE %:feedCodeSuffix")
	Long getEventIdByFeedCodeSuffix(String feedCodeSuffix);
}
